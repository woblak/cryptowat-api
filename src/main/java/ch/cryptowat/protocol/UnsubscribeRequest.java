package ch.cryptowat.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UnsubscribeRequest {

    private Unsubscribe unsubscribe;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class Unsubscribe {

        private List<Subscription> subscriptions;

        @NoArgsConstructor
        @AllArgsConstructor
        @Getter
        public static class Subscription {
            private StreamSubscription streamSubscription;

            @NoArgsConstructor
            @AllArgsConstructor
            @Getter
            public static class StreamSubscription {
                private String resource;
            }
        }
    }
}

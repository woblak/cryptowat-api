package ch.cryptowat.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class TradeRecord {

    private MarketUpdate marketUpdate;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    public static class MarketUpdate {
        private Market market;
        private TradesUpdate tradesUpdate;

        @NoArgsConstructor
        @AllArgsConstructor
        @Getter
        public static class Market {
            private String exchangeId;
            private String currencyPairId;
            private String marketId;
        }

        @NoArgsConstructor
        @AllArgsConstructor
        @Getter
        public static class TradesUpdate {
            private List<Trade> trades;

            @NoArgsConstructor
            @AllArgsConstructor
            @Getter
            public static class Trade {
                private String externalId;
                private String timestamp;
                private String timestampNano;
                private String priceStr;
                private String amountStr;
                private String amountQuoteStr;
                private String orderSide;
            }
        }
    }
}
